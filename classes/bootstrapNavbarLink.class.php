<?php
/**
* @package frontend
* @subpackage bootstrap
* @author Florian Gadal
* @copyright 2013 Coffee Ring Prod
* @link http://coffeeringprod.fr
* @license All right reserved
*/
class bootstrapNavbarLink {
	
	public $id;
	public $label;
	public $url;
	public $order;

	function __construct($id, $label, $url, $order = 0) {

		$this->id = $id;
		$this->label = $label;
		$this->url = $url;
		$this->order = $order;
	}
}

function bootstrapNavbarLinkSort($linkA, $linkB) {

	return ($linkA->order - $linkB->order);
}

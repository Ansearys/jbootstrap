<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="#">{$APPNAME}</a>
			<div class="nav-collapse">
				<ul class="nav">
					{zone 'jbootstrap~navbar', array('active' => $active)}
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>

<div class="container">
	{$MAIN}

  	<hr>

	<footer>
		<p>&copy; {$APPNAME} {$DATE}</p>
	</footer>
</div> <!-- /container -->

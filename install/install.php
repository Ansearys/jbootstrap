<?php
/**
* @package   frontend
* @subpackage bootstrap
* @author    Florian Gadal
* @copyright 2012 Coffee Ring Prod
* @link      http://coffeeringprod.fr
* @license    All rights reserved
*/


class jbootstrapModuleInstaller extends jInstallerModule {

    function install() {
    	if ($this->firstExec('copyResponse')) {
        	$this->copyDirectoryContent('responses/', jApp::appPath('responses'));
        }
    }

}

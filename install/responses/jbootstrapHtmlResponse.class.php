<?php
/**
* @package   bootstrap
* @subpackage 
* @author    Florian Gadal
* @copyright 2012 Coffee Ring Prod
* @link      http://coffeeringprod.fr
* @license    All rights reserved
*/


require_once (JELIX_LIB_CORE_PATH.'response/jResponseHtml.class.php');

class jbootstrapHtmlResponse extends jResponseHtml {

    public $bodyTpl = 'jbootstrap~main';

    function __construct() {
        parent::__construct();

        $this->addJsLink(jApp::config()->urlengine['jqueryPath'] . 'jquery.js');

        $this->addCSSLink(jUrl::get('jelix~www:getfile', array('targetmodule' => 'jbootstrap', 'file' => 'css/bootstrap.css')));
        $this->addJsLink(jUrl::get('jelix~www:getfile', array('targetmodule' => 'jbootstrap', 'file' => 'js/bootstrap.js')));
    }

    protected function doAfterActions() {
        $this->body->assignIfNone('MAIN','<p>no content</p>');  
        $this->body->assignIfNone('active', '');

        $this->body->assign('APPNAME', 'Jelix');
        $this->body->assign('DATE', date('Y'));
    }
}
